//Choice 1 and 2 for Ford Internship

#include <iostream>
#include <cmath>
#include <string>
#include <cassert>

using namespace std;

//choice 1
int MDistance(int a, int b, int c, int d)
{
    //Caclulate the two distances
    int xdis = abs(a-c);
    int ydis = abs(b-d);
    //Adds the distances together
    return xdis + ydis;
}

//choice 2
string MBgui(int input)
{
    //Temporary variable
    string hold = "\0";
    //checks if divisible by 3
    if(input % 3 == 0)
    {
        hold += "Mustang";
    }
    //checks if divisible by 5
    if(input % 5 == 0)
    {
        hold += "Bronco";
    }
    //If not divisible returns the number
    if(hold == "\0")
    {
        hold = to_string(input);
    }
    return hold;
}

//Runs the tests for the two choices
int main()
{
    //Assert runs automatic tests while the outputs allow for corrections
    cout<<"Testing choice 1\n";
    cout<<"A(5,4) and B(3,2)\n";
    cout<<"Output: " << MDistance(5,4,3,2) << "; should be 4\n";
    assert(MDistance(5,4,3,2)  == 4);
    cout<<"A(-1,2) and B(1,-2)\n";
    cout<<"Output: " << MDistance(-1,2,1,-2) << "; should be 6\n\n";
    assert(MDistance(-1,2,1,-2)  == 6);
    
    cout<<"Testing choice 2\n";
    cout<<"Input: 2\n";
    cout<<"Output: "<<MBgui(2)<<"\n";
    assert(MBgui(2) == "2");
    cout<<"Input: 6\n";
    cout<<"Output: "<<MBgui(6)<<"\n";
    assert(MBgui(6) == "Mustang");
    cout<<"Input: 10\n";
    cout<<"Output: "<<MBgui(10)<<"\n";
    assert(MBgui(10) == "Bronco");
    cout<<"Input: 15\n";
    cout<<"Output: "<<MBgui(15)<<"\n";
    assert(MBgui(15) == "MustangBronco");

    return 0;
}